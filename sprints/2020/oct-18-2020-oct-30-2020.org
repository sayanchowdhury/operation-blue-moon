#+TITLE: October 18-30, 2020 (13 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 13
  :SPRINTSTART: <2020-10-18 Sun>
  :wpd-akshay196: 1
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :wpd-jasonbraganza: 5
  :wpd-sandeepk: 2
  :END:
** akshay196
*** DONE Watch justforfunc videos - Part II [6/22]
    CLOSED: [2020-10-27 Tue 21:23]
    :PROPERTIES:
    :ESTIMATED: 13
    :ACTUAL:   8.12
    :OWNER:    akshay196
    :ID:       READ.1601831181
    :TASKID:   READ.1601831181
    :END:
    :LOGBOOK:
    CLOCK: [2020-10-27 Tue 20:21]--[2020-10-27 Tue 21:23] =>  1:02
    CLOCK: [2020-10-26 Mon 14:25]--[2020-10-26 Mon 18:30] =>  4:05
    CLOCK: [2020-10-23 Fri 20:19]--[2020-10-23 Fri 21:16] =>  0:57
    CLOCK: [2020-10-22 Thu 19:46]--[2020-10-22 Thu 20:11] =>  0:25
    CLOCK: [2020-10-21 Wed 19:29]--[2020-10-21 Wed 20:27] =>  0:58
    CLOCK: [2020-10-20 Tue 22:26]--[2020-10-20 Tue 23:06] =>  0:40
    :END:
    https://www.youtube.com/playlist?list=PL64wiCrrxh4Jisi7OcCJIUpguV_f5jGnZ
    - [X] justforfunc #12: a Text to Speech server with gRPC and Kubernetes                (50m)
    - [X] justforfunc #13: more text to speech with cgo and Docker multistage builds! (4k) (50m)
    - [X] justforfunc #14: a twitter bot and systemd (that runs free on GCP)               (40m)
    - [X] justforfunc #15: a code review with logging, errors, and signals                 (40m)
    - [X] justforfunc #16: unit testing HTTP servers                                       (50m)
    - [X] justforfunc #17: contributing to the Go project                                  (45m)
** bhavin192
*** DONE Site Reliability Engineering - III. Practices - Part IV [4/4]
    CLOSED: [2020-10-29 Thu 20:20]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   6.37
    :OWNER:    bhavin192
    :ID:       READ.1596455869
    :TASKID:   READ.1596455869
    :END:
    :LOGBOOK:
    CLOCK: [2020-10-29 Thu 20:01]--[2020-10-29 Thu 20:20] =>  0:19
    CLOCK: [2020-10-28 Wed 21:03]--[2020-10-28 Wed 21:28] =>  0:25
    CLOCK: [2020-10-28 Wed 20:33]--[2020-10-28 Wed 21:03] =>  0:30
    CLOCK: [2020-10-27 Tue 22:07]--[2020-10-27 Tue 22:28] =>  0:21
    CLOCK: [2020-10-27 Tue 21:12]--[2020-10-27 Tue 21:23] =>  0:11
    CLOCK: [2020-10-25 Sun 22:17]--[2020-10-25 Sun 22:31] =>  0:14
    CLOCK: [2020-10-25 Sun 20:37]--[2020-10-25 Sun 20:58] =>  0:21
    CLOCK: [2020-10-24 Sat 20:57]--[2020-10-24 Sat 21:10] =>  0:13
    CLOCK: [2020-10-24 Sat 20:08]--[2020-10-24 Sat 20:55] =>  0:47
    CLOCK: [2020-10-23 Fri 21:02]--[2020-10-23 Fri 21:05] =>  0:03
    CLOCK: [2020-10-23 Fri 20:39]--[2020-10-23 Fri 21:02] =>  0:23
    CLOCK: [2020-10-22 Thu 20:21]--[2020-10-22 Thu 20:52] =>  0:31
    CLOCK: [2020-10-21 Wed 20:46]--[2020-10-21 Wed 21:09] =>  0:23
    CLOCK: [2020-10-21 Wed 20:09]--[2020-10-21 Wed 20:46] =>  0:37
    CLOCK: [2020-10-20 Tue 22:40]--[2020-10-20 Tue 23:14] =>  0:34
    CLOCK: [2020-10-19 Mon 20:40]--[2020-10-19 Mon 21:10] =>  0:30
    :END:
    https://landing.google.com/sre/sre-book/toc/index.html
    - [X] 14. Managing Incidents                                                   (2h)
    - [X] 15. Postmortem Culture: Learning from Failure                            (2h)
    - [X] 16. Tracking Outages                                                     (2h)
    - [X] 17. Testing for Reliability - Part I                                     (2h)
** gandalfdwite
*** DONE Container Security by Liz Rice [4/4]
    CLOSED: [2020-10-28 Wed 12:39]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   8.00
    :OWNER: gandalfdwite
    :ID: READ.1601269860
    :TASKID: READ.1601269860
    :END:
    :LOGBOOK:
    CLOCK: [2020-10-26 Mon 21:40]--[2020-10-26 Mon 22:45] =>  1:05
    CLOCK: [2020-10-25 Sun 11:00]--[2020-10-25 Sun 12:00] =>  1:00
    CLOCK: [2020-10-24 Sat 14:00]--[2020-10-24 Sat 15:05] =>  1:05
    CLOCK: [2020-10-22 Thu 21:15]--[2020-10-22 Thu 22:30] =>  1:15
    CLOCK: [2020-10-21 Wed 20:00]--[2020-10-21 Wed 21:00] =>  1:00
    CLOCK: [2020-10-20 Tue 22:50]--[2020-10-21 Wed 00:00] =>  1:10
    CLOCK: [2020-10-18 Sun 12:30]--[2020-10-18 Sun 13:55] =>  1:25
    :END:
    - [X] Container Security Threats                 ( 2h )
    - [X] Linux System Calls, Perm, Capabilities     ( 2h )
    - [X] Control Groups                             ( 1h )
    - [X] Container Isolation                        ( 3h )

*** DONE Blog post on Argo [1/1]
    CLOSED: [2020-11-03 Tue 18:25]
    :PROPERTIES:
    :ESTIMATED: 5
    :ACTUAL:   5.42
    :OWNER: gandalfdwite
    :ID: WRITE.1602856081
    :TASKID: WRITE.1602856081
    :END:
    - [X] Write blog post on Argo CI/CD    ( 5h )
    CLOCK: [2020-11-01 Sun 13:10]--[2020-11-01 Sun 15:15] =>  2:05
    CLOCK: [2020-10-29 Thu 21:10]--[2020-10-29 Thu 23:00] =>  1:50
    CLOCK: [2020-10-28 Wed 19:30]--[2020-10-28 Wed 21:00] =>  1:30
** jasonbraganza
*** TODO BEGLA 136 - English At The Work Place - Part I
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:
    :OWNER: jasonbraganza
    :ID: READ.1602813945
    :TASKID: READ.1602813945
    :END:
	- [ ] BLOCK 01 - Exploring the Job Market
	- [ ] Unit 01 - Profiling Oneself for the Job
	- [ ] Unit 02 - Searching for a Job
	- [ ] Unit 03 - Responding to Job Advertisements
	- [ ] Unit 04 - Preparing for the Job
	- [ ] BLOCK 02 - Preparing for Job Interviews
	- [ ] Unit 05 - Using Body Language Effectively
	- [ ] Unit 06 - Portfolio Making
	- [ ] Unit 07 - Writing Your Curriculum Vitae (CV)
	- [ ] Unit 08 - Preparing to Face an Interview
	- [ ] BLOCK 03 - Skills for the Workplace - I
	- [ ] Unit 09 - Etiquette, Cultural Awareness and Gender Perception
	- [ ] Unit 10 - Understanding Customers
	- [ ] Unit 11 - Essentials of Customer Service
	- [ ] Unit 12 - Work Ethics
	- [ ] BLOCK 04 - Skills for the Workplace - II
	- [ ] Unit 13 - Participating in Discussions
	- [ ] Unit 14 - Making Presentations
	- [ ] Unit 15 - Writing Business Letters
	- [ ] Unit 16 - Writing Business E-mails
*** DONE Complete all of Reuven Lerner’s exercises courses + books - Part II [100%]
    CLOSED: [2020-10-29 Thu 14:36]
    :PROPERTIES:
    :ESTIMATED: 12
    :ACTUAL:   11.33
    :OWNER: jasonbraganza
    :ID: DEV.1600409922
    :TASKID: DEV.1600409922
    :END:
    :LOGBOOK:
    CLOCK: [2020-10-29 Thu 09:30]--[2020-10-29 Thu 14:36] =>  5:06
    CLOCK: [2020-10-24 Sat 11:22]--[2020-10-24 Sat 13:35] =>  2:13
    CLOCK: [2020-10-24 Sat 10:30]--[2020-10-24 Sat 11:00] =>  0:30
    CLOCK: [2020-10-22 Thu 08:43]--[2020-10-22 Thu 12:14] =>  3:31
    :END:
**** DONE iterators (Total 13) [9/9]
     - [X] Exercise 5
     - [X] Exercise 6
     - [X] Exercise 7
     - [X] Exercise 8
     - [X] Exercise 9
     - [X] Exercise 10
     - [X] Exercise 11
     - [X] Exercise 12
     - [X] Exercise 13

*** DONE Read Python Book [100%]
    CLOSED: [2020-10-22 Thu 18:25]
    :PROPERTIES:
    :ESTIMATED: 8
    :ACTUAL:   3.42
    :OWNER: jasonbraganza
    :ID: READ.1603345955
    :TASKID: READ.1603345955
    :END:
    :LOGBOOK:
    CLOCK: [2020-10-22 Thu 15:36]--[2020-10-22 Thu 18:25] =>  2:49
    CLOCK: [2020-10-22 Thu 12:13]--[2020-10-22 Thu 12:49] =>  0:36
    :END:
  - [X] Chapter 1
  - [X] Chapter 2
  - [X] Chapter 3
  - [X] Chapter 4
  - [X] Chapter 5
  - [X] Chapter 6
  - [X] Chapter 7
  - [X] Chapter 8
  - [X] Chapter 9
  - [X] Chapter 10
  - [X] Chapter 11
  - [X] Chapter 12
  - [X] Chapter 13
  - [X] Chapter 14
  - [X] Chapter 15
  - [X] Chapter 16
  - [X] Chapter 17

** sandeepk
*** DONE Math for programmers [100%]
    :PROPERTIES:
    :ESTIMATED: 2
    :ACTUAL:   1.50
    :OWNER: sandeepk
    :ID: READ.1603042265
    :TASKID: READ.1603042265
    :END:
    :LOGBOOK:
    CLOCK: [2020-10-22 Thu 22:30]--[2020-10-22 Thu 23:00] =>  0:30
    CLOCK: [2020-10-21 Wed 21:30]--[2020-10-21 Wed 22:00] =>  0:30
    CLOCK: [2020-10-20 Tue 21:30]--[2020-10-20 Tue 22:00] =>  0:30
    :END:
    - [X] Chapter 1 - Prime Numbers         ( 2hr )

