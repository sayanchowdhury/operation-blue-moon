#+TITLE: May 7-23, 2020 (17 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 17
  :SPRINTSTART: <2020-05-07 Thu>
  :wpd-akshay196: 2
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :wpd-sandeepk: 1
  :END:
** akshay196
*** DONE The Go Programming Language - Part II
    CLOSED: [2020-05-23 Sat 17:49]
    :PROPERTIES:
    :ESTIMATED:  34
    :ACTUAL:   19.02
    :OWNER: akshay196
    :ID: READ.1587566538
    :TASKID: READ.1587566538
    :END:
    :LOGBOOK:
    CLOCK: [2020-05-23 Sat 16:01]--[2020-05-23 Sat 17:49] =>  1:48
    CLOCK: [2020-05-23 Sat 08:11]--[2020-05-23 Sat 09:41] =>  1:30
    CLOCK: [2020-05-22 Fri 10:24]--[2020-05-22 Fri 11:49] =>  1:25
    CLOCK: [2020-05-21 Thu 20:08]--[2020-05-21 Thu 20:48] =>  0:40
    CLOCK: [2020-05-20 Wed 08:27]--[2020-05-20 Wed 09:09] =>  0:42
    CLOCK: [2020-05-19 Tue 17:01]--[2020-05-19 Tue 18:15] =>  1:14
    CLOCK: [2020-05-19 Tue 03:11]--[2020-05-19 Tue 03:50] =>  0:39
    CLOCK: [2020-05-18 Mon 18:53]--[2020-05-18 Mon 19:17] =>  0:24
    CLOCK: [2020-05-18 Mon 15:29]--[2020-05-18 Mon 16:53] =>  1:24
    CLOCK: [2020-05-18 Mon 09:32]--[2020-05-18 Mon 09:45] =>  0:13
    CLOCK: [2020-05-17 Sun 18:30]--[2020-05-17 Sun 19:24] =>  0:54
    CLOCK: [2020-05-17 Sun 10:12]--[2020-05-17 Sun 10:23] =>  0:11
    CLOCK: [2020-05-17 Sun 08:41]--[2020-05-17 Sun 09:25] =>  0:44
    CLOCK: [2020-05-15 Fri 08:39]--[2020-05-15 Fri 09:58] =>  1:19
    CLOCK: [2020-05-14 Thu 07:32]--[2020-05-14 Thu 08:03] =>  0:31
    CLOCK: [2020-05-13 Wed 18:16]--[2020-05-13 Wed 18:57] =>  0:41
    CLOCK: [2020-05-12 Tue 14:37]--[2020-05-12 Tue 15:11] =>  0:34
    CLOCK: [2020-05-11 Mon 14:03]--[2020-05-11 Mon 14:33] =>  0:30
    CLOCK: [2020-05-10 Sun 20:46]--[2020-05-10 Sun 21:26] =>  0:40
    CLOCK: [2020-05-10 Sun 08:30]--[2020-05-10 Sun 09:10] =>  0:40
    CLOCK: [2020-05-09 Sat 20:22]--[2020-05-09 Sat 20:49] =>  0:27
    CLOCK: [2020-05-08 Fri 08:52]--[2020-05-08 Fri 09:45] =>  0:53
    CLOCK: [2020-05-07 Thu 18:48]--[2020-05-07 Thu 19:46] =>  0:58
    :END:
    https://learning.oreilly.com/library/view/the-go-programming/9780134190570/
    - [X] Chapter 4.3. Maps                                                 (120m)
    - [X] Chapter 4.4. Structs                                              (120m)
    - [X] Chapter 4.5. JSON                                                 (120m)
    - [X] Chapter 4.6. Text and HTML Templates                              (120m)
    - [X] Chapter 5.   Functions                                            ( 15m)
    - [X] Chapter 5.1. Function Declarations                                ( 60m)
    - [X] Chapter 5.2. Recursion                                            (120m)
    - [X] Chapter 5.3. Multiple Return Values                               (120m)
    - [X] Chapter 5.4. Errors                                               ( 90m)
    - [X] Chapter 5.5. Function Values                                      (120m)
    - [X] Chapter 5.6. Anonymous Functions                                  (120m)
    - [X] Chapter 5.7. Variadic Functions                                   (120m)
    - [X] Chapter 5.8. Deferred Function Calls                              (120m)
    - [X] Chapter 5.9. Panic                                                ( 90m)
    - [X] Chapter 5.10. Recover                                             (120m)
    - [X] Chapter 6.   Methods                                              ( 15m)
** bhavin192
*** DONE Practical Go Modules
    CLOSED: [2020-05-09 Sat 00:44]
    :PROPERTIES:
    :ESTIMATED: 4
    :ACTUAL:   4.22
    :OWNER:    bhavin192
    :ID:       READ.1588869527
    :TASKID:   READ.1588869527
    :END:
    :LOGBOOK:
    CLOCK: [2020-05-08 Fri 20:31]--[2020-05-09 Sat 00:44] =>  4:13
    :END:
    https://learning.oreilly.com/live-training/courses/practical-go-modules/0636920374275/
*** DONE Write blog post about jq
    CLOSED: [2020-05-12 Tue 21:31]
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:   6.63
    :OWNER:    bhavin192
    :ID:       WRITE.1586698969
    :TASKID:   WRITE.1586698969
    :END:
    :LOGBOOK:
    CLOCK: [2020-05-12 Tue 20:40]--[2020-05-12 Tue 21:31] =>  0:51
    CLOCK: [2020-05-11 Mon 21:29]--[2020-05-11 Mon 22:01] =>  0:32
    CLOCK: [2020-05-11 Mon 20:51]--[2020-05-11 Mon 20:57] =>  0:06
    CLOCK: [2020-05-11 Mon 20:28]--[2020-05-11 Mon 20:49] =>  0:21
    CLOCK: [2020-05-10 Sun 15:22]--[2020-05-10 Sun 18:53] =>  3:31
    CLOCK: [2020-05-07 Thu 20:01]--[2020-05-07 Thu 21:18] =>  1:17
    :END:
*** DONE Programming Kubernetes - 1. Introduction
    CLOSED: [2020-05-20 Wed 21:08]
    :PROPERTIES:
    :ESTIMATED: 5.5
    :ACTUAL:   4.67
    :OWNER:    bhavin192
    :ID:       READ.1588871088
    :TASKID:   READ.1588871088
    :END:
    :LOGBOOK:
    CLOCK: [2020-05-20 Wed 20:46]--[2020-05-20 Wed 21:08] =>  0:22
    CLOCK: [2020-05-19 Tue 22:03]--[2020-05-19 Tue 22:35] =>  0:32
    CLOCK: [2020-05-19 Tue 20:47]--[2020-05-19 Tue 21:17] =>  0:30
    CLOCK: [2020-05-18 Mon 22:18]--[2020-05-18 Mon 22:25] =>  0:07
    CLOCK: [2020-05-17 Sun 15:43]--[2020-05-17 Sun 16:11] =>  0:28
    CLOCK: [2020-05-17 Sun 13:56]--[2020-05-17 Sun 14:25] =>  0:29
    CLOCK: [2020-05-17 Sun 13:10]--[2020-05-17 Sun 13:19] =>  0:09
    CLOCK: [2020-05-17 Sun 12:24]--[2020-05-17 Sun 13:07] =>  0:43
    CLOCK: [2020-05-14 Thu 22:55]--[2020-05-14 Thu 23:10] =>  0:15
    CLOCK: [2020-05-13 Wed 22:17]--[2020-05-13 Wed 22:41] =>  0:24
    CLOCK: [2020-05-13 Wed 21:00]--[2020-05-13 Wed 21:22] =>  0:22
    CLOCK: [2020-05-13 Wed 20:14]--[2020-05-13 Wed 20:33] =>  0:19
    :END:
    https://learning.oreilly.com/library/view/programming-kubernetes/9781492047094/
*** DONE CNCF Member Webinar: The KUbernetes Test TooL (kuttl)
    CLOSED: [2020-05-23 Sat 23:40]
    :PROPERTIES:
    :ESTIMATED: 1.5
    :ACTUAL:   0.93
    :OWNER:    bhavin192
    :ID:       READ.1588871586
    :TASKID:   READ.1588871586
    :END:
    :LOGBOOK:
    CLOCK: [2020-05-23 Sat 22:44]--[2020-05-23 Sat 23:40] =>  0:56
    :END:
    https://www.cncf.io/webinars/the-kubernetes-test-tool-kuttl/

** gandalfdwite
*** DONE Prometheus: Up & Running - Part II [6/6]
    CLOSED: [2020-05-23 Sat 19:43]
    :PROPERTIES:
    :ESTIMATED: 17
    :ACTUAL:   17.87
    :OWNER: gandalfdwite
    :ID: READ.1587476760
    :TASKID: READ.1587476760
    :END:
    :LOGBOOK:
    CLOCK: [2020-05-22 Fri 18:46]--[2020-05-22 Fri 19:41] =>  0:55
    CLOCK: [2020-05-22 Fri 08:35]--[2020-05-22 Fri 09:45] =>  1:10
    CLOCK: [2020-05-21 Thu 23:00]--[2020-05-22 Fri 00:05] =>  1:05
    CLOCK: [2020-05-20 Wed 20:20]--[2020-05-20 Wed 21:35] =>  1:15
    CLOCK: [2020-05-19 Tue 21:54]--[2020-05-19 Tue 23:09] =>  1:15
    CLOCK: [2020-05-18 Mon 22:47]--[2020-05-19 Tue 00:31] =>  1:44
    CLOCK: [2020-05-16 Sat 22:25]--[2020-05-16 Sat 23:20] =>  0:55
    CLOCK: [2020-05-15 Fri 15:18]--[2020-05-15 Fri 16:45] =>  1:27
    CLOCK: [2020-05-13 Wed 22:58]--[2020-05-14 Thu 00:30] =>  1:32
    CLOCK: [2020-05-11 Mon 18:36]--[2020-05-11 Mon 19:48] =>  1:12
    CLOCK: [2020-05-10 Sun 11:10]--[2020-05-10 Sun 13:10] =>  2:00
    CLOCK: [2020-05-09 Sat 08:23]--[2020-05-09 Sat 09:10] =>  0:47
    CLOCK: [2020-05-08 Fri 19:05]--[2020-05-08 Fri 20:10] =>  1:05
    CLOCK: [2020-05-07 Thu 21:40]--[2020-05-07 Thu 23:10] =>  1:30
    :END:
    - [X] 6. Dashboarding With Grafana                        ( 4h )
    - [X] 7. Node Exporter                                    ( 3h )
    - [X] 8. Service Discovery                                ( 3h )
    - [X] 9. Containers and Kubernetes                        ( 3h )
    - [X] 10. Common Exporters                                ( 2h )
    - [X] 11. Working with other Monitoring Systems           ( 2h )

** sandeepk
*** DONE Book Fluent Python Part IV [4/4]
    CLOSED: [2020-05-23 Sat 21:30]
    :PROPERTIES:
    :ESTIMATED: 17
    :ACTUAL:   9.08
    :OWNER: sandeepk
    :ID: READ.1585286321
    :TASKID: READ.1585286321
    :END:
    :LOGBOOK:
    CLOCK: [2020-05-23 Sat 20:50]--[2020-05-23 Sat 21:30] =>  0:40
    CLOCK: [2020-05-23 Sat 14:35]--[2020-05-23 Sat 15:00] =>  0:25
    CLOCK: [2020-05-22 Fri 20:45]--[2020-05-22 Fri 21:30] =>  0:45
    CLOCK: [2020-05-21 Thu 21:40]--[2020-05-21 Thu 22:30] =>  0:50
    CLOCK: [2020-05-20 Wed 09:35]--[2020-05-20 Wed 10:20] =>  0:45
    CLOCK: [2020-05-19 Tue 21:00]--[2020-05-19 Tue 21:50] =>  0:50
    CLOCK: [2020-05-17 Sun 15:05]--[2020-05-17 Sun 15:40] =>  0:35
    CLOCK: [2020-05-16 Sat 21:15]--[2020-05-16 Sat 21:35] =>  0:20
    CLOCK: [2020-05-15 Fri 09:15]--[2020-05-15 Fri 09:30] =>  0:15
    CLOCK: [2020-05-14 Thu 22:20]--[2020-05-14 Thu 23:00] =>  0:40
    CLOCK: [2020-05-14 Thu 17:40]--[2020-05-14 Thu 18:40] =>  1:00
    CLOCK: [2020-05-13 Wed 22:50]--[2020-05-13 Wed 23:10] =>  0:20
    CLOCK: [2020-05-09 Sat 18:45]--[2020-05-09 Sat 19:55] =>  1:10
    CLOCK: [2020-05-09 Sat 13:25]--[2020-05-09 Sat 13:55] =>  0:30
    :END:
    - [X] Chapter 13. Operator Overloading: Doing It Right ( 4 hr )
    - [X] Chapter 14. Iterables, Iterators, and Generators ( 5 hr )
    - [X] Chapter 15. Context Managers and else Blocks     ( 4 hr )
    - [X] Chapter 16. Coroutines                           ( 4 hr )

