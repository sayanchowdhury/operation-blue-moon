#+TITLE: Jun 22-July 4, 2020 (13 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 13
  :SPRINTSTART: <2020-06-22 Mon>
  :wpd-bhavin192: 1
  :wpd-gandalfdwite: 1
  :END:
** bhavin192
*** DONE Programming Kubernetes - 7. Shipping Controllers and Operators
    CLOSED: [2020-06-24 Wed 23:30]
    :PROPERTIES:
    :ESTIMATED: 3
    :ACTUAL:   2.03
    :OWNER:    bhavin192
    :ID:       READ.1588871088
    :TASKID:   READ.1588871088
    :END:
    :LOGBOOK:
    CLOCK: [2020-06-24 Wed 22:24]--[2020-06-24 Wed 23:30] =>  1:06
    CLOCK: [2020-06-24 Wed 20:20]--[2020-06-24 Wed 20:27] =>  0:07
    CLOCK: [2020-06-24 Wed 19:48]--[2020-06-24 Wed 20:08] =>  0:20
    CLOCK: [2020-06-22 Mon 22:34]--[2020-06-22 Mon 23:03] =>  0:29
    :END:
    https://learning.oreilly.com/library/view/programming-kubernetes/9781492047094/
*** DONE Programming Kubernetes - 8. Custom API Servers - I
    CLOSED: [2020-07-04 Sat 17:52]
    :PROPERTIES:
    :ESTIMATED: 10
    :ACTUAL:   10.78
    :OWNER:    bhavin192
    :ID:       READ.1588871088
    :TASKID:   READ.1588871088
    :END:
    :LOGBOOK:
    CLOCK: [2020-07-04 Sat 17:33]--[2020-07-04 Sat 17:52] =>  0:19
    CLOCK: [2020-07-04 Sat 16:46]--[2020-07-04 Sat 17:21] =>  0:35
    CLOCK: [2020-07-04 Sat 15:47]--[2020-07-04 Sat 16:19] =>  0:32
    CLOCK: [2020-07-04 Sat 14:48]--[2020-07-04 Sat 15:14] =>  0:26
    CLOCK: [2020-07-04 Sat 12:44]--[2020-07-04 Sat 13:48] =>  1:04
    CLOCK: [2020-07-03 Fri 20:15]--[2020-07-03 Fri 21:24] =>  1:09
    CLOCK: [2020-07-03 Fri 18:45]--[2020-07-03 Fri 19:55] =>  1:10
    CLOCK: [2020-07-03 Fri 16:51]--[2020-07-03 Fri 17:44] =>  0:53
    CLOCK: [2020-07-02 Thu 21:04]--[2020-07-02 Thu 21:16] =>  0:12
    CLOCK: [2020-07-01 Wed 22:37]--[2020-07-01 Wed 23:13] =>  0:36
    CLOCK: [2020-06-30 Tue 19:51]--[2020-06-30 Tue 21:06] =>  1:15
    CLOCK: [2020-06-29 Mon 20:20]--[2020-06-29 Mon 21:16] =>  0:56
    CLOCK: [2020-06-28 Sun 18:35]--[2020-06-28 Sun 19:37] =>  1:02
    CLOCK: [2020-06-27 Sat 20:39]--[2020-06-27 Sat 21:17] =>  0:38
    :END:
    https://learning.oreilly.com/library/view/programming-kubernetes/9781492047094/

** gandalfdwite
*** DONE Prometheus: Up & Running - Part IV [6/6]
    CLOSED: [2020-07-04 Sat 10:28]
    :PROPERTIES:
    :ESTIMATED: 13
    :ACTUAL:   13.60
    :OWNER: gandalfdwite
    :ID: READ.1587476760
    :TASKID: READ.1587476760
    :END:
    :LOGBOOK:
    CLOCK: [2020-07-04 Sat 08:47]--[2020-07-04 Sat 09:50] =>  1:03
    CLOCK: [2020-07-02 Thu 10:52]--[2020-07-02 Thu 12:13] =>  1:21
    CLOCK: [2020-06-30 Tue 20:16]--[2020-06-30 Tue 21:31] =>  1:15
    CLOCK: [2020-06-29 Mon 18:54]--[2020-06-29 Mon 20:00] =>  1:06
    CLOCK: [2020-06-28 Sun 14:00]--[2020-06-28 Sun 15:20] =>  1:20
    CLOCK: [2020-06-27 Sat 11:05]--[2020-06-27 Sat 12:45] =>  1:40
    CLOCK: [2020-06-26 Fri 20:29]--[2020-06-26 Fri 22:24] =>  1:55
    CLOCK: [2020-06-25 Thu 22:57]--[2020-06-25 Thu 23:58] =>  1:01
    CLOCK: [2020-06-24 Wed 21:37]--[2020-06-24 Wed 22:01] =>  0:24
    CLOCK: [2020-06-24 Wed 16:12]--[2020-06-24 Wed 17:23] =>  1:11
    CLOCK: [2020-06-23 Tue 20:25]--[2020-06-23 Tue 21:45] =>  1:20
    :END:
    - [X] 14. Aggregation Operators                           ( 3h )
    - [X] 15. Binary Operators                                ( 2h )
    - [X] 16. Functions                                       ( 2h )
    - [X] 17. Recording Rules                                 ( 2h )
    - [X] 18. Alerting                                        ( 2h )
    - [X] 19. Alertmanager                                    ( 2h )

